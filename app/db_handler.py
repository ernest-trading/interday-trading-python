import yaml
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, sessionmaker, joinedload

from sql_database import *


with open('./config.yml') as file:
    content = yaml.load(file, Loader=yaml.FullLoader)


def start_db_session():
    db_dir = content['database_directory']
    # # For this example we will use an in-memory sqlite DB.
    # # Let's also configure it to echo everything it does to the screen.
    # engine = create_engine(db_dir, echo=True)
    #
    # # The base class which our objects will be defined on.
    # Base = declarative_base()
    #
    # # Create all tables by issuing CREATE TABLE commands to the DB.
    # Base.metadata.create_all(engine)
    #
    # # Creates a new session to the database by using the engine we described.
    # Session = sessionmaker(bind=engine)
    # session = Session()
    database_engine = create_engine(db_dir, echo=True)


def save_backtest_ideas_db(strategy_code: str, description: str, stop_loss: str, entry_point: str,
                           profit_level: str, no_of_bars: int, action: str):
    backtest_ideas = BacktestIdeas(**{'strategy_code':  strategy_code,
                                      'description': description,
                                      'stop_loss': stop_loss,
                                      'entry_point': entry_point,
                                      'profit_level': profit_level,
                                      'no_of_bars': no_of_bars,
                                      'action': action})


def save_backtest_performance_db():
    pass
