import logging

from sqlalchemy import create_engine, MetaData, Table, Column, ForeignKey
from sqlalchemy.orm import sessionmaker
from sql_database import *


def fetch_existing_idea_strategy_codes():
    # TODO: Replace the path to be the ENV variable or inside a configuration file.
    engine = create_engine('sqlite:///./playground.db')

    Session = sessionmaker(bind=engine)
    session = Session()

    bis = session.query(BacktestIdeas).all()
    existing_strategy_codes = [bi.strategy_code for bi in bis]

    return existing_strategy_codes


def fetch_existing_performance_strategy_codes():
    # TODO: Replace the path to be the ENV variable or inside a configuration file.
    engine = create_engine('sqlite:///./playground.db')

    Session = sessionmaker(bind=engine)
    session = Session()

    bis = session.query(BacktestPerformance).all()
    existing_strategy_codes = [bp.strategy_code for bp in bis]

    return existing_strategy_codes


def get_logger(name="root", loglevel=logging.INFO):
    logger = logging.getLogger(name)
    logger.setLevel(loglevel)
    ch = logging.StreamHandler()
    ch.setLevel(loglevel)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s.%(funcName)s - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)

    for h in logger.handlers:
        logger.removeHandler(h)

    logger.addHandler(ch)

    return logger


__all__ = (
    'fetch_existing_idea_strategy_codes',
    'fetch_existing_performance_strategy_codes',
    'get_logger'
)
