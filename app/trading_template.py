import time
import logging

import pandas as pd
import yaml
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from fileconverterpy.util import from_csv_to_json, json_extractor
from technicalanalysispy.candlestick import Candlestick
from performanceanalysispy.performance_matrices import PerformanceMatrices
from networkandutilitiespy.utils import custom_logger
from backtestutilitiespy.util import *

from sql_database import Base, BacktestPerformance, BacktestIdeas
from app.util import fetch_existing_idea_strategy_codes, fetch_existing_performance_strategy_codes


log = custom_logger(__name__, filename='my_logs', mode='w+', log_level=logging.INFO)    # Output .log file

with open('./config.yml') as file:
    content = yaml.load(file, Loader=yaml.FullLoader)

EXISTING_IDEA_STRATEGY_CODES = fetch_existing_idea_strategy_codes()
EXISTING_PERFORMANCE_STRATEGY_CODES = fetch_existing_performance_strategy_codes()

db_dir = content['database_directory']
database_engine = create_engine(db_dir, echo=True)
# The Session can be eliminated?
Session = sessionmaker(bind=database_engine)
session = Session()
database_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=database_engine))
Base.query = database_session.query_property()


class ErnestTrading:

    def __init__(self, data_file_path, strategy_idea_csv_path, expected_json_path, instrument_symbol):
        self.__strategy_idea_csv_path = strategy_idea_csv_path
        self.__instrument_symbol = instrument_symbol
        self.__expected_json_path = expected_json_path
        from_csv_to_json(csv_path=self.__strategy_idea_csv_path,
                         expected_json_path=self.__expected_json_path,
                         outer_keys=False)
        self.__content = json_extractor(json_path=self.__expected_json_path)
        self.__df = pd.read_csv(data_file_path)

    def fetch_instrument_symbol(self):
        return self.__instrument_symbol

    def fetch_backtest_ideas(self):
        return self.__content

    @staticmethod
    def __save_backtest_ideas_db(strategy_code: str, description: str, interval: str, stop_loss: str,
                                 entry_point: str, profit_level: str, no_of_bars: int, action: str,
                                 instrument_symbol: str, bar_resolution: str):
        backtest_ideas = BacktestIdeas(**{'strategy_code': strategy_code, 'description': description,
                                          'interval': interval, 'stop_loss': stop_loss,
                                          'entry_point': entry_point, 'profit_level': profit_level,
                                          'no_of_bars': no_of_bars, 'action': action,
                                          'instrument_symbol': instrument_symbol, 'bar_resolution': bar_resolution})
        database_session.add(backtest_ideas)
        database_session.commit()

    @staticmethod
    def __save_backtest_performance_db(strategy_code: str, ernest_ratio: float, sharpe_ratio: float,
                                       annual_return: float, sortino_ratio: float, calmar_ratio: float,
                                       maxi_drawdown: float, frequency: int, win_rate: float,
                                       time_to_recover: int, description: str, last_backtest_time: int):
        backtest_performance = BacktestPerformance(**{'strategy_code': strategy_code, 'ernest_ratio': ernest_ratio,
                                                      'sharpe_ratio': sharpe_ratio, 'annual_return': annual_return,
                                                      'sortino_ratio': sortino_ratio, 'calmar_ratio': calmar_ratio,
                                                      'maxi_drawdown': maxi_drawdown, 'frequency': frequency,
                                                      'win_rate': win_rate, 'time_to_recover': time_to_recover,
                                                      'description': description, 'last_backtest_time': last_backtest_time})
        database_session.add(backtest_performance)
        database_session.commit()

    @staticmethod
    def __fetch_description(strategy_code):
        # TODO: code, return the description of the strategy based on its strategy code.
        for row in session.query(BacktestPerformance).filter_by(StrategyCode=strategy_code):
            return row.description

    def __check_if_backtest_idea_strategy_code_exists(self, existing_strategy_code, idea_interval,
                                                      idea_instrument_symbol, idea_action,
                                                      idea_no_of_bars, idea_bar_resolution,
                                                      idea_description):
        interval, random_code, instrument_symbol, action, no_of_bars, bar_resolution = \
            strategy_code_decomposition(strategy_code=existing_strategy_code)

        if idea_interval == interval and idea_instrument_symbol == instrument_symbol and idea_action == action \
                and idea_no_of_bars == no_of_bars and idea_bar_resolution == bar_resolution:
            if self.__fetch_description(existing_strategy_code) == idea_description:
                proposed_strategy_code = existing_strategy_code
                duplicated = True

                return duplicated, proposed_strategy_code
            else:
                proposed_strategy_code = self.__generate_unique_strategy_code(
                    idea_interval=idea_interval, idea_instrument_symbol=idea_instrument_symbol,
                    idea_action=idea_action, idea_no_of_bars=idea_no_of_bars, idea_bar_resolution=idea_bar_resolution)
                duplicated = False

                return duplicated, proposed_strategy_code
        else:
            proposed_strategy_code = self.__generate_unique_strategy_code(
                idea_interval=idea_interval, idea_instrument_symbol=idea_instrument_symbol,
                idea_action=idea_action, idea_no_of_bars=idea_no_of_bars, idea_bar_resolution=idea_bar_resolution)
            duplicated = False

            return duplicated, proposed_strategy_code

    @staticmethod
    def __generate_unique_strategy_code(idea_interval, idea_instrument_symbol, idea_action,
                                        idea_no_of_bars, idea_bar_resolution):
        temp_strategy_code = generate_backtest_code(
                             resolution=idea_interval, no_of_eng_char=4, no_of_digits=8,
                             instrument_code=idea_instrument_symbol, direction=idea_action,
                             timeframe=f'{idea_no_of_bars}{idea_bar_resolution}')
        while True:
            if temp_strategy_code not in EXISTING_IDEA_STRATEGY_CODES:
                return temp_strategy_code
            else:
                temp_strategy_code = generate_backtest_code(
                    resolution=idea_interval, no_of_eng_char=4, no_of_digits=8,
                    instrument_code=idea_instrument_symbol, direction=idea_action,
                    timeframe=f'{idea_no_of_bars}{idea_bar_resolution}')

    @staticmethod
    def export_backtest_performance_report():
        bps = BacktestPerformance.query.all()
        # f = open()

    def __start(self):
        candle = Candlestick(df=self.__df)

        self.__daily_pct_change = candle.c.pct_change()
        self.__signal = pd.Series(0, index=candle.df.index)
        self.__positions = pd.Series(0, index=candle.df.index)
        self.__pnl_series = pd.Series(0.0, index=candle.df.index)

        return candle

    def __check_entry_at_close(self, strategy_number: int):
        return True if self.__content[strategy_number]['entry_point'].split('.')[1][0] == 'c' else False

    def __calculate_position_pnl(self, direction, candle: Candlestick, i, stop_loss,
                                 entry_point, profit_level, no_of_bars, ratio):
        if direction == 'L':
            success = False
            target_level = entry_point + ratio * profit_level
            pos = 1
            for nb in range(1, no_of_bars+1):
                self.__positions[i + nb] += pos

                if candle.l[i + nb] < stop_loss:
                    self.__pnl_series[i + nb] += (stop_loss - candle.c[i + nb - 1])/candle.c[i + nb - 1]
                    success = False
                    log.info(f'[UPDATES] It hits the stop loss.')
                    break
                elif candle.h[i + nb] >= target_level:
                    self.__pnl_series[i + nb] += (target_level - candle.c[i + nb - 1])/candle.c[i + nb - 1]
                    success = True
                    log.info(f'[UPDATES] It is successful.')
                    break
                else:
                    self.__pnl_series[i + nb] += (candle.c[i + nb] - candle.c[i + nb - 1])/candle.c[i + nb - 1]

                if nb == no_of_bars:
                    log.info(f'[UPDATES] It never hits the target level.')
                    success = False

            return success

        elif direction == 'S':
            success = False
            target_level = entry_point - ratio * profit_level
            pos = -1
            for nb in range(1, no_of_bars+1):
                self.__positions[i + nb] += pos

                if candle.h[i + nb] > stop_loss:
                    self.__pnl_series[i + nb] += (candle.c[i + nb - 1] - stop_loss)/candle.c[i + nb - 1]
                    success = False
                    log.info(f'[UPDATES] It hits the stop loss.')
                    break
                elif candle.l[i + nb] <= target_level:
                    self.__pnl_series[i + nb] += (candle.c[i + nb - 1] - target_level)/candle.c[i + nb - 1]
                    success = True
                    log.info(f'[UPDATES] It is successful.')
                    break
                else:
                    self.__pnl_series[i + nb] += (candle.c[i + nb] - candle.c[i + nb - 1])/candle.c[i + nb - 1]

                if nb == no_of_bars:
                    log.info(f'[UPDATES] It never hits the target level.')
                    success = False

            return success

        else:
            log.error(f'The direction parameter is invalid.')

            return None

    def single_run(self, strategy_number: int):
        candle = self.__start()
        if self.__check_entry_at_close(strategy_number=strategy_number):
            # Create/Find the right strategy code.
            strategy_code = ''
            for no, existing_code in enumerate(EXISTING_IDEA_STRATEGY_CODES):
                duplicated, proposed_strategy_code = \
                    self.__check_if_backtest_idea_strategy_code_exists(
                        existing_strategy_code=existing_code,
                        idea_interval=self.__content[strategy_number]['interval'],
                        idea_instrument_symbol=self.__content[strategy_number]['instrument_symbol'],
                        idea_action=self.__content[strategy_number]['action'],
                        idea_no_of_bars=self.__content[strategy_number]['no_of_bars'],
                        idea_bar_resolution=self.__content[strategy_number]['bar_resolution'],
                        idea_description=self.__content[strategy_number]['description'])
                if duplicated:
                    strategy_code = proposed_strategy_code
                    break

                if no == len(EXISTING_IDEA_STRATEGY_CODES) - 1:
                    strategy_code = proposed_strategy_code
                    # Save to the database.
                    self.__save_backtest_ideas_db(
                        strategy_code=strategy_code,
                        description=self.__content[strategy_number]['description'],
                        interval=self.__content[strategy_number]['interval'],
                        stop_loss=self.__content[strategy_number]['stop_loss'],
                        entry_point=self.__content[strategy_number]['entry_point'],
                        profit_level=self.__content[strategy_number]['profit_level'],
                        no_of_bars=self.__content[strategy_number]['no_of_bars'],
                        action=self.__content[strategy_number]['action'],
                        instrument_symbol=self.__content[strategy_number]['instrument_symbol'],
                        bar_resolution=self.__content[strategy_number]['bar_resolution'])

            if self.__content[strategy_number]['action'] == 'L':
                unix_timestamp = int(time.time())
                for i in range(10, candle.no_of_bars - self.__content[strategy_number]['no_of_bars']):
                    if eval(self.__content[strategy_number]['description']):
                        self.__signal[i] = -1
                        result = self.__calculate_position_pnl(
                            direction='L', candle=candle, i=i,
                            stop_loss=eval(self.__content[strategy_number]['stop_loss']),
                            entry_point=eval(self.__content[strategy_number]['entry_point']),
                            profit_level=eval(self.__content[strategy_number]['profit_level']),
                            no_of_bars=self.__content[strategy_number]['no_of_bars'], ratio=1)

                        if result:
                            self.__signal[i] = 1
                        else:
                            self.__signal[i] = -1

                pm = PerformanceMatrices(pnl=self.__pnl_series)

                # TODO: Check if there is existing strategy code, yes -> update; no -> add new columns
                if strategy_code not in EXISTING_PERFORMANCE_STRATEGY_CODES:
                    # TODO: Some are not accurately filled.
                    self.__save_backtest_performance_db(
                        strategy_code=strategy_code,
                        ernest_ratio=0.0,
                        sharpe_ratio=pm.fetch_annualized_sharpe_ratio(),
                        annual_return=pm.fetch_annualized_return(),
                        sortino_ratio=pm.fetch_sortino_ratio(),
                        calmar_ratio=pm.fetch_calmar_ratio(),
                        maxi_drawdown=pm.fetch_maximum_drawdown(),
                        frequency=999,
                        win_rate=999,
                        time_to_recover=999,
                        description=self.__content[strategy_number]['description'],
                        last_backtest_time=unix_timestamp)
                else:
                    # TODO: Update the database.
                    pass
            elif self.__content[strategy_number]['action'] == 'S':
                unix_timestamp = int(time.time())
                for i in range(10, candle.no_of_bars - self.__content[strategy_number]['no_of_bars']):
                    if eval(self.__content[strategy_number]['description']):
                        self.__signal[i] = -1
                        result = self.__calculate_position_pnl(
                            direction='S', candle=candle, i=i,
                            stop_loss=eval(self.__content[strategy_number]['stop_loss']),
                            entry_point=eval(self.__content[strategy_number]['entry_point']),
                            profit_level=eval(self.__content[strategy_number]['profit_level']),
                            no_of_bars=self.__content[strategy_number]['no_of_bars'], ratio=1)

                        if result:
                            self.__signal[i] = 1
                        else:
                            self.__signal[i] = -1

                pm = PerformanceMatrices(pnl=self.__pnl_series)

                # TODO: Check if there is existing strategy code, yes -> update; no -> add new columns
                if strategy_code not in EXISTING_PERFORMANCE_STRATEGY_CODES:
                    # TODO: Some are not accurately filled.
                    self.__save_backtest_performance_db(
                        strategy_code=strategy_code,
                        ernest_ratio=0.0,
                        sharpe_ratio=pm.fetch_annualized_sharpe_ratio(),
                        annual_return=pm.fetch_annualized_return(),
                        sortino_ratio=pm.fetch_sortino_ratio(),
                        calmar_ratio=pm.fetch_calmar_ratio(),
                        maxi_drawdown=pm.fetch_maximum_drawdown(),
                        frequency=999,
                        win_rate=999,
                        time_to_recover=999,
                        description=self.__content[strategy_number]['description'],
                        last_backtest_time=unix_timestamp)
                else:
                    # TODO: Update the database.
                    pass

                pm.performance_plot()

            else:
                log.error(f'The direction can either only be LONG or SHORT.')

        else:
            log.info(f'The Entry point is not at Close.')
