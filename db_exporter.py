from sql_database import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sql_database import *


database_engine = create_engine('sqlite:///./playground.db')
database_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=database_engine))

Base.query = database_session.query_property()

bp = BacktestPerformance.query.all()

print(BacktestPerformance.__table__.columns.keys())
print(BacktestPerformance)


# import csv
# import sqlalchemy as sqAl
#
# metadata = sqAl.MetaData()
# engine = sqAl.create_engine('sqlite:///./playground.db')
# metadata.bind = engine
#
# mytable = sqAl.Table('backtest_performance', metadata, autoload=True)
# db_connection = engine.connect()
#
# select = sqAl.sql.select([mytable])
# result = db_connection.execute(select)
#
# fh = open('data.csv', 'wb')
# outcsv = csv.writer(fh)
#
# # outcsv.writerow(result.keys())
# outcsv.writerows(result)
