import yaml
import re


def fetch_interval(strategy_code):
    for i in range(len(strategy_code)):
        if strategy_code[i].isalpha():
            return strategy_code[0:i+1]


strategy_code = '15mAqfj52513396#HSIF*L21D'
# s = re.split('[a-z|,]+', strategy_code)
temp = strategy_code.split('*')
instrument_symbol = temp[0].split('#')[1]
action = temp[1][0]
interval = fetch_interval(strategy_code)
no_of_bars = temp[1][1:-1]
bar_resolution = temp[1][-1]
random_code = temp[0].split('#')[0][len(interval):]

print(interval, instrument_symbol, action, no_of_bars, bar_resolution, random_code)



