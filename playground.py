'''
import yaml
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, sessionmaker, joinedload

from .sql_database import *


with open('./config.yml') as file:
    content = yaml.load(file, Loader=yaml.FullLoader)


def start_db_session():
    db_dir = content['database_directory']
    # For this example we will use an in-memory sqlite DB.
    # Let's also configure it to echo everything it does to the screen.
    engine = create_engine(db_dir, echo=True)

    # The base class which our objects will be defined on.
    Base = declarative_base()

    # Create all tables by issuing CREATE TABLE commands to the DB.
    Base.metadata.create_all(engine)

    # Creates a new session to the database by using the engine we described.
    Session = sessionmaker(bind=engine)
    session = Session()

    return session

'''

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from sql_database import *

#
# database_engine = create_engine('sqlite:///./history.db')
# database_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=database_engine))
#
# Base.query = database_session.query_property()


# def __save_backtest_ideas_db(strategy_code: str, description: str, stop_loss: str, entry_point: str,
#                              profit_level: str, no_of_bars: int, action: str):
#     backtest_ideas = BacktestIdeas(**{'strategy_code': strategy_code,
#                                       'description': description,
#                                       'stop_loss': stop_loss,
#                                       'entry_point': entry_point,
#                                       'profit_level': profit_level,
#                                       'no_of_bars': no_of_bars,
#                                       'action': action})
#     database_session.add(backtest_ideas)
#     database_session.commit()
#
#
# def __save_backtest_performance_db(strategy_code: str, ernest_ratio: float, sharpe_ratio: float,
#                                    annual_return: float, sortino_ratio: float, calmar_ratio: float,
#                                    maxi_drawdown: float, frequency: int, win_rate: float,
#                                    time_to_recover: int, description: str, last_backtest_time: int):
#     backtest_performance = BacktestPerformance(**{'strategy_code': strategy_code, 'ernest_ratio': ernest_ratio,
#                                                   'sharpe_ratio': sharpe_ratio, 'annual_return': annual_return,
#                                                   'sortino_ratio': sortino_ratio, 'calmar_ratio': calmar_ratio,
#                                                   'maxi_drawdown': maxi_drawdown, 'frequency': frequency,
#                                                   'win_rate': win_rate, 'time_to_recover': time_to_recover,
#                                                   'description': description, 'last_backtest_time': last_backtest_time})
#     database_session.add(backtest_performance)
#     database_session.commit()
# #
# #
# for i in range(10):
#     __save_backtest_ideas_db(str(i), 's', '123', '55', '45', 25, 'LONG')

from sqlalchemy import Column, Integer, String, ForeignKey, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, sessionmaker, joinedload

# For this example we will use an in-memory sqlite DB.
# Let's also configure it to echo everything it does to the screen.
engine = create_engine('sqlite:///helloworld.db', echo=True)

# The base class which our objects will be defined on.
Base = declarative_base()


# Create all tables by issuing CREATE TABLE commands to the DB.
Base.metadata.create_all(engine)

# Creates a new session to the database by using the engine we described.
Session = sessionmaker(bind=engine)
session = Session()

