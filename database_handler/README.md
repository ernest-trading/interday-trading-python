```python
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

database_engine = create_engine('sqlite:///./history.db')
Session = sessionmaker(bind=database_engine)
database_session = Session()


country = CryptoTrade(**{'id': '30624700',
                         'action': True,
                         'base_currency': 'BTC',
                         'quote_currency': 'USDT',
                         'order_quantity': '0.036',
                         'price': '9500',
                         'filled_quantity': '0.025',
                         'exchange': 'latoken',
})

database_session.add(country)
database_session.commit()
```