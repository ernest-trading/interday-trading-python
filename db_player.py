from sql_database import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sql_database import *


def create_db(name='sqlite:///./playground.db'):
    database_engine = create_engine(name)
    Session = sessionmaker(bind=database_engine)
    database_session = Session()

    Base.metadata.drop_all(bind=database_engine)
    Base.metadata.create_all(bind=database_engine)

    database_session.commit()


# create_db()
# def __save_backtest_ideas_db(strategy_code: str, description: str, stop_loss: str, entry_point: str,
#                              profit_level: str, no_of_bars: int, action: str):
#     backtest_ideas = BacktestIdeas(**{'strategy_code': strategy_code,
#                                       'description': description,
#                                       'stop_loss': stop_loss,
#                                       'entry_point': entry_point,
#                                       'profit_level': profit_level,
#                                       'no_of_bars': no_of_bars,
#                                       'action': action})
#     database_session.add(backtest_ideas)
#     database_session.commit()


def __save_backtest_performance_db(strategy_code: str, ernest_ratio: float, sharpe_ratio: float,
                                   annual_return: float, sortino_ratio: float, calmar_ratio: float,
                                   maxi_drawdown: float, frequency: int, win_rate: float,
                                   time_to_recover: int, description: str, last_backtest_time: int):
    backtest_performance = BacktestPerformance(**{'strategy_code': strategy_code, 'ernest_ratio': ernest_ratio,
                                                  'sharpe_ratio': sharpe_ratio, 'annual_return': annual_return,
                                                  'sortino_ratio': sortino_ratio, 'calmar_ratio': calmar_ratio,
                                                  'maxi_drawdown': maxi_drawdown, 'frequency': frequency,
                                                  'win_rate': win_rate, 'time_to_recover': time_to_recover,
                                                  'description': description, 'last_backtest_time': last_backtest_time})
    database_session.add(backtest_performance)
    database_session.commit()


database_engine = create_engine('sqlite:///./playground.db')
database_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=database_engine))

Base.query = database_session.query_property()
#
# for i in range(100):
#     __save_backtest_performance_db(str(i), 1.1, 2.1, 5.5, 6.2, 1.2, 8, 6, 0.5, 6, 'haha', 12134123)
#

bp = BacktestPerformance.query.all()
print(BacktestPerformance.__table__.columns.keys())


# import csv
# outfile = open('mydump.csv', 'wb')
#
# outcsv = csv.writer(outfile)
#
# outcsv.writerow(bp.keys())
# outcsv.writerow(bp)
# outfile.close()


import csv

def export():

    # q = BacktestPerformance.query
    #
    # file = './states.csv'
    #
    # with open(file, 'w') as csvfile:
    #     outcsv = csv.writer(csvfile, delimiter=',',quotechar='"', quoting = csv.QUOTE_MINIMAL)
    #
    #     header = BacktestPerformance.__table__.columns.keys()
    #
    #     outcsv.writerow(header)
    #
    #     for record in q.all():
    #         outcsv.writerow([getattr(record, c) for c in header ])
    database_engine = create_engine('sqlite:///./playground.db')
    Session = sessionmaker(bind=database_engine)
    database_session = Session()

    import csv
    outfile = open('mydump.csv', 'wb')
    outcsv = csv.writer(outfile)
    records = database_session.query(BacktestPerformance).all()
    [outcsv.writerow([getattr(curr, column.name) for column in BacktestPerformance.__mapper__.columns]) for curr in records]
    # or maybe use outcsv.writerows(records)

    outfile.close()

if __name__ == "__main__":
    export()

