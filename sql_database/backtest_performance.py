from .base import Base

from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, BINARY, FLOAT, BOOLEAN, DECIMAL


class BacktestPerformance(Base):
    __tablename__ = 'backtest_performance'

    strategy_code = Column('StrategyCode', String, primary_key=True)
    ernest_ratio = Column('ErnestRatio', FLOAT, nullable=False)
    sharpe_ratio = Column('SharpeRatio', FLOAT, nullable=False)
    annual_return = Column('AnnualReturn', FLOAT, nullable=False)
    sortino_ratio = Column('SortinoRatio', FLOAT, nullable=False)
    calmar_ratio = Column('CalmarRatio', FLOAT, nullable=False)
    maxi_drawdown = Column('MaxiDrawdown', FLOAT, nullable=False)
    frequency = Column('Frequency', Integer, nullable=False)
    win_rate = Column('WinRate', FLOAT, nullable=False)
    time_to_recover = Column('TimeToRecover', Integer, nullable=False)
    description = Column('Description', String, nullable=False)
    last_backtest_time = Column('LastBacktestTime', Integer, nullable=False)

    def __init__(self, strategy_code, ernest_ratio, sharpe_ratio, annual_return, sortino_ratio, calmar_ratio,
                 maxi_drawdown, frequency, win_rate, time_to_recover, description, last_backtest_time):
        self.strategy_code = strategy_code
        self.ernest_ratio = ernest_ratio
        self.sharpe_ratio = sharpe_ratio
        self.annual_return = annual_return
        self.sortino_ratio = sortino_ratio
        self.calmar_ratio = calmar_ratio
        self.maxi_drawdown = maxi_drawdown
        self.frequency = frequency
        self.win_rate = win_rate
        self.time_to_recover = time_to_recover
        self.description = description
        self.last_backtest_time = last_backtest_time

    def __repr__(self):
        return "<BacktestPerformance(strategy_code='%s', ernest_ratio='%s', sharpe_ratio='%s', annual_return='%s', " \
               "sortino_ratio='%s', calmar_ratio='%s', maxi_drawdown='%s', frequency='%s', win_rate='%s', " \
               "time_to_recover='%s', description='%s', last_backtest_time='%s')>" % \
               (self.strategy_code, self.ernest_ratio, self.sharpe_ratio, self.annual_return, self.sortino_ratio,
                self.calmar_ratio, self.maxi_drawdown, self.frequency, self.win_rate, self.time_to_recover,
                self.description, self.last_backtest_time)


__all__ = (
    'BacktestPerformance',
)
