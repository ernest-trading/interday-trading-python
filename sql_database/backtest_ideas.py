from .base import Base

from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, BINARY, FLOAT, BOOLEAN, DECIMAL


class BacktestIdeas(Base):
    __tablename__ = 'backtest_ideas'

    strategy_code = Column('StrategyCode', String, primary_key=True)
    description = Column('Description', String, nullable=False)
    interval = Column('Interval', String, nullable=False)
    stop_loss = Column('StopLoss', String, nullable=False)
    entry_point = Column('EntryPoint', String, nullable=False)
    profit_level = Column('ProfitLevel', String, nullable=False)
    no_of_bars = Column('NoOfBars', Integer, nullable=False)
    action = Column('Action', String, nullable=False)
    instrument_symbol = Column('InstrumentSymbol', String, nullable=False)
    bar_resolution = Column('BarResolution', String, nullable=False)

    def __init__(self, strategy_code, description, interval, stop_loss, entry_point, profit_level,
                 no_of_bars, action, instrument_symbol, bar_resolution):
        self.strategy_code = strategy_code
        self.description = description
        self.interval = interval
        self.stop_loss = stop_loss
        self.entry_point = entry_point
        self.profit_level = profit_level
        self.no_of_bars = no_of_bars
        self.action = action
        self.instrument_symbol = instrument_symbol
        self.bar_resolution = bar_resolution

    def __repr__(self):
        return "<BacktestIdeas(strategy_code='%s', description='%s', interval='%s', stop_loss='%s', " \
               "entry_point='%s', profit_level='%s', no_of_bars='%s', action='%s', " \
               "instrument_symbol='%s', bar_resolution='%s')>" % \
               (self.strategy_code, self.description, self.interval, self.stop_loss, self.entry_point,
                self.profit_level, self.no_of_bars, self.action, self.instrument_symbol, self.bar_resolution)


__all__ = (
    'BacktestIdeas',
)
